import settings

import os
from time import sleep

from selenium import webdriver
from selenium.webdriver.edge.options import Options
from selenium.webdriver.edge.service import Service
from webdriver_manager.microsoft import EdgeChromiumDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests
from discord import Webhook, RequestsWebhookAdapter
from discord_webhook import DiscordWebhook, DiscordEmbed

# XPATH
BOX_USERNAME = "/html/body/table/tbody/tr[2]/td[1]/form/table/tbody/tr[1]/td[2]/input"
BOX_PASSWORD = "/html/body/table/tbody/tr[2]/td[1]/form/table/tbody/tr[2]/td[2]/input[1]"
BTN_LOGIN = "/html/body/table/tbody/tr[2]/td[1]/form/p/input"
BTN_STAND_ANZEIGEN = "/html/body/table/tbody/tr/td[1]/form/div/input[10]"
TEXT_POINTS_TABLE = "/html/body/table[2]/tbody/tr[1]/th[4]"


def sendDiscordMessage():
    webhook = DiscordWebhook(url=settings.DISCORD_WEBHOOK_URL)
    embed = DiscordEmbed(title="Analysis-Ergebnisse verfügbar :robot: :rocket:", color=16719907,
                         description="Click title to see results", url=settings.ANALYSIS_URL)
    webhook.add_embed(embed)
    webhook.execute()


def checkForAvailableResult():
    # Init
    options = Options()
    service = Service(EdgeChromiumDriverManager().install())
    driver = webdriver.Edge(options=options, service=service)

    driver.get(settings.ANALYSIS_URL)
    driver.maximize_window()

    # Login
    username = WebDriverWait(driver, 10).until(
        EC.visibility_of_element_located((By.XPATH, BOX_USERNAME)))
    username.send_keys(settings.USERNAME)
    password = driver.find_element(By.XPATH, BOX_PASSWORD)
    password.send_keys(settings.PASSWORD)
    login = driver.find_element(By.XPATH, BTN_LOGIN)
    login.click()

    # Switch to points-overview
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, BTN_STAND_ANZEIGEN))).click()

    # Check if a Table can be found at the given position
    try:
        text = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, TEXT_POINTS_TABLE))).text
        if (text != "Gueltig"):
            # No Result available
            raise WebDriverWait
        # Result available
        print("Result found")

        # Send message once
        if os.stat(settings.STATUS_FILE).st_size == 0:
            print("Sending Discord-Message")
            sendDiscordMessage()
            file = open(settings.STATUS_FILE, "w")
            file.write("Result available")

    except:
        # no Result available
        print("No Results found")
    
    driver.quit()
    return 0


if __name__ == '__main__':
    checkForAvailableResult()
